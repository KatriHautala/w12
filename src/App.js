import './App.css';

function App() {
  //using <br> as space between some of the spans for lazy reasons, usage of padding shown in css
  // view suitable for mobile use
  return ( 
    <div className="App">
      <img src="https://www.vamk.fi/fi/meidan_vamk/korkeakoulu/medialle/materiaalit/graafinen_ohjeisto/logo-keski.jpg" alt="logo"></img><br/>
      <span class="name">
        Katri Hautala
      </span>
      <span class="profession"><br/>
        Student
      </span>
      <span class="major"><br/>
        Information Technology
      </span>
      <span class="school"><br/><br/><br/>
        School of Technology
      </span>
      <span class="contact"><br/><br/><br/>
        katri.hautala92@gmail.com <br/>
        +358 45 807 4333
      </span> 
      <span> <br/><br/>
        Wolffintie 30, FI-65200 VAASA, Finland
      </span> 
    </div> 
  );
}

export default App;
